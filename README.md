# Zip Software Engineer Interview

## Overview

Zip is a payment gateway that lets consumers split purchases into 4 interest free installments, every two weeks. The first 25% is taken when the purchase is made, and the remaining 3 installments of 25% are automatically taken every 14 days. We help customers manage their cash-flow while helping merchants increase conversion rates and average order values.

It may help to see our [product in action online](https://www.fanatics.com/mlb/new-york-yankees/new-york-yankees-nike-home-replica-custom-jersey-white/o-8976+t-36446587+p-2520909211+z-8-3193055640?_ref=p-CLP:m-GRID:i-r0c1:po-1), checkout our app on [ios](https://apps.apple.com/us/app/quadpay-buy-now-pay-later/id1425045070) or [android](https://play.google.com/store/apps/details?id=com.quadpay.quadpay&hl=en_US), and to read our documentation (https://docs.us.zip.co).

## Background

One of the cornerstones of Zip's culture is openness and transparency. When reviewing our existing interview structure, we found that pair-programming challenges rarely replicated what our employees actually do in their day-to-day work. For example, when was the last time you coded without google, or when the requirements weren't clearly defined? To tackle that, we've decided to publish our pair programming interview and share it directly with candidates beforehand.

As an Engineer at Zip you’ll help solve interesting problems on a daily basis. Some areas that you'll work on include fraud prevention, building real-time credit-decisioning models and, most importantly, shipping products that are secure, frictionless, and deliver a high-quality consumer experience.

The pair programming challenge will take an hour, and will more closely replicate a day-in-the-life at Zip. You’re free to use whichever resources help you to get the job done. When we evaluate your code at the end of the session, we will be looking for: 
- A high code health
- Simplicity
- Readability
- Presence of tests or planning for future tests
- And maintainability

While we mainly use .NET and C# in our back-end, we welcome candidates who are more familiar with other languages. We ask that you simply confirm your language with your recruiter beforehand. At the moment, we have only finalized starter code for C#, but feel free to look through that to prepare for your assignment even if using another language.

## The Pair Programming Interview

### The Challenge

During the interview, you will build a core service for our business, an Installment Calculator. There is no need to build anything before the interview, but feel free to investigate the boilerplate code and do some research on how you would set this up.

#### Installment Calculator
##### User Story

As a Zip Customer, I would like to establish a payment plan spread over 6 weeks that splits the original charge evenly over 4 installments.

##### Acceptance Criteria
- Given it is the 1st of January, 2020
- When I create an order of $100.00
- And I select 4 Installments
- And I select a frequency of 14 days
- Then I should be charged these 4 installments
  - `01/01/2020   -   $25.00`
  - `01/15/2020   -   $25.00`
  - `01/29/2020   -   $25.00`
  - `02/12/2020   -   $25.00`

## Assumption:
- Minimum value for purchase amount will be $ 1.
- Zip API return object in JSON format as below.
  ```
  {
	"id": "54bac5bf-efa7-477a-bdbc-f75d1d4747f3",
	"purchaseAmount": 5000,
	"installments": [{
			"id": "37efb367-47d0-4f2f-9efe-a30f547be814",
			"dueDate": "2022-10-31T00:00:00+05:30",
			"amount": 1250
		},
		{
			"id": "70ce7e6e-1270-45de-8e4d-8bb69a111f8c",
			"dueDate": "2022-11-14T00:00:00+05:30",
			"amount": 1250
		},
		{
			"id": "afd1ba7f-b6e9-4061-904a-f44608aca835",
			"dueDate": "2022-11-28T00:00:00+05:30",
			"amount": 1250
		},
		{
			"id": "6d3a8af4-da82-450a-bde8-d238fde41fee",
			"dueDate": "2022-12-12T00:00:00+05:30",
			"amount": 1250
		}]
  }
  ```
- Number of installment, freqency, and installment start is handled throw configurations.

## Design:
- This system is designed using multilevel architecture.
- At base level system has Zip.Installments.Common, which contains model definations and custom exceptions.
- On top of Zip.Installments.Common, system has Zip.InstallmentsService, which contains service for creating purchase plan.
- On top of Zip.InstallmentsService, system has Zip.InstallmentsAPI which contains public API's for creating purchase plan.
- All these system modules where tested by comman Zip.InstallmentsService.Test.

## Techonology Used:
 - .Net 5
 - Serilog.AspNetCore
 - Serilog.Sinks.Console
 - Serilog.Sinks.File
 - Swashbuckle.AspNetCore
 - FluentValidation.AspNetCore

## Prerequisite To Run Code:
- VS 2019
- .Net 5 SDK
- Postman
 
 ## How to run code in VS 2019?
 - Download or clone code from this repo.
 - Open VS 2019.
 - Browse solution file from cloned folder.
 - Set Zip.InstallmentsAPI as startup project.
 - Validate `InstallmentSetting` in appsettings.json. It should be as below
    ```
    "InstallmentSetting": 
    {
     "Installments": 4,
     "Frequency": 14,
     "StartDay ": 0
    }
    ```
- Validate `Serilog` in appsettings.json. It should be as below
    ```
    "Serilog": 
    {
      "MinimumLevel": 
      {
        "Default": "Information",
        "Microsoft": "Information",
        "Microsoft.Hosting.Lifetime": "Information"
	    },
	  "Using": ["Serilog.Sinks.Console", "Serilog.Sinks.File"],
	  "WriteTo":[{
            "Name": "Console"
        },
        {
          "Name": "File",
          "Args":
          {
            "path": "./bin/logs/log.txt",
            "rollingInterval": "Day"
          }
        }
      ]
    }
    ```
- Once everything looks fine, Run project.
- System will open swagger in browser window.
- Click on /api/PaymentPlan/GetPaymentPlan.
- System will expand the tab
- In tab, click on Try it out button.
- Enter Purchase amount in request body and click on execute button.
- In case of success, system will response as below.
   ![image](https://user-images.githubusercontent.com/41372705/199034470-0851e15c-0d0d-4093-91c6-604c1806a8a4.png)
   
## Running Unit Test Cases:
- In VS 2019, Click View --> select Test Explorer
- System will open window as below
  ![image](https://user-images.githubusercontent.com/41372705/199035803-8595c075-b178-47e1-bbb5-4f2fd995e44b.png)
- Right click on Zip.InstallmentsService.Test and select run.

## CURL Request for API
```
curl --location --request POST 'https://localhost:5001/api/PaymentPlan/GetPaymentPlan' \
--header 'accept: */*' \
--header 'Content-Type: application/json' \
--data-raw '{"id":"3fa85f64-5717-4562-b3fc-2c963f66afa6","purchaseAmount":52.25}'
```