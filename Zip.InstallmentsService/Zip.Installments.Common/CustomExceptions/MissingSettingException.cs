﻿using System;

namespace Zip.Installments.Common.CustomExceptions
{
    /// <summary>
    /// This class is responsible for missing setting exception.
    /// </summary>
    public class MissingSettingException : Exception
    {
        #region Constructor

        /// <summary>
        /// Creates instance of <see cref="MissingSettingException"/>
        /// </summary>
        /// <param name="settingType">Sets the setting type</param>
        public MissingSettingException(string settingType) : base($"Missing {settingType} settings.")
        {
        }

        #endregion
    }
}
