using System.Text.Json;

namespace Zip.Installments.Common.Model
{
    /// <summary>
    /// Used to hold error information
    /// </summary>
    public class ErrorInformation
    {
        /// <summary>
        /// Creates instance of <see cref="ErrorInformation"/>
        /// </summary>
        /// <param name="statusCode">Status code</param>
        /// <param name="message">Error message</param>
        /// <param name="details">Error details</param>
        public ErrorInformation(int statusCode, string message, string details = "")
        {
            StatusCode = statusCode;
            Message = message;
            Details = details;
        }

        /// <summary>
        /// Gets or sets the status code
        /// </summary>
        public int StatusCode { get; set; }

        /// <summary>
        /// Gets or sets the error message
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Gets or sets the error details
        /// </summary>
        public string Details { get; set; }

        /// <summary>
        /// Overridden method to convert <see cref="ErrorInformation"/> to json string
        /// </summary>
        /// <returns>JSON string of <see cref="ErrorInformation"/></returns>
        public override string ToString()
        {
            return JsonSerializer.Serialize(this);
        }
    }
}
