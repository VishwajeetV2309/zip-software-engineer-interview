﻿namespace Zip.Installments.Common.Model
{
    /// <summary>
    /// Data structure which defines all the properties for an installment setting.
    /// </summary>
    public class InstallmentSetting
    {
        /// <summary>
        /// Gets or sets the number of installment
        /// </summary>
        public int Installments { get; set; }

        /// <summary>
        /// Gets or sets the frequency in number of days for installment
        /// </summary>
        public int Frequency { get; set; }

        /// <summary>
        /// Gets or sets the Start day in number for installment
        /// 0: Installment will start from today.
        /// 1: Installment will start from tommarow.
        /// </summary>
        public int StartDay { get; set; }
    }
}
