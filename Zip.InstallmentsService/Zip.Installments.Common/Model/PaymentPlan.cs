﻿using System;

namespace Zip.Installments.Common.Model
{
    /// <summary>
    /// Data structure which defines all the properties for a purchase installment plan.
    /// </summary>
    public class PaymentPlan : Purchase
    {
        /// <summary>
        /// Creates instance of <see cref="PaymentPlan"/>
        /// </summary>
        /// <param name="id">The Guid value</param>
        /// <param name="purchaseAmount">Purchase amount in decimal</param>
        public PaymentPlan(Guid id, decimal purchaseAmount) : base(id, purchaseAmount)
        {
        }

        /// <summary>
        /// Gets or sets the installments breakdown
        /// <see cref="Installment[]"/>
        /// </summary>
        public Installment[] Installments { get; set; }
    }
}
