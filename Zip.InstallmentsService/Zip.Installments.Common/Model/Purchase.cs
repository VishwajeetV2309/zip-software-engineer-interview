using FluentValidation;
using System;
using System.Text.Json;

namespace Zip.Installments.Common.Model
{
    /// <summary>
    /// Data structure which defines all the properties for a purchase.
    /// </summary>
    public class Purchase
    {
        /// <summary>
        /// Creates instance of <see cref="Purchase"/>
        /// </summary>
        /// <param name="id">The Guid value</param>
        /// <param name="purchaseAmount">Purchase amount in decimal</param>
        public Purchase(Guid id, decimal purchaseAmount)
        {
            Id = id == default ? Guid.NewGuid() : id;
            PurchaseAmount = purchaseAmount;
        }

        /// <summary>
        /// Gets or sets the unquie Identifier for payment plan
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the Purchase amount for the payment plan
        /// </summary>
        public decimal PurchaseAmount { get; set; }

        /// <summary>
        /// Overridden method to convert <see cref="Purchase"/> to json string
        /// </summary>
        /// <returns>JSON string of <see cref="Purchase"/></returns>
        public override string ToString()
        {
            return JsonSerializer.Serialize(this);
        }
    }

    /// <summary>
    /// This class used to add validation settings for <see cref="Purchase"/>
    /// </summary>
    public class PurchaseValidator : AbstractValidator<Purchase>
    {
        /// <summary>
        /// Creates instance of <see cref="PurchaseValidator"/>
        /// </summary>
        public PurchaseValidator()
        {
            RuleFor(model => model.PurchaseAmount).NotEmpty()
                .GreaterThanOrEqualTo(1m)
                .WithMessage("Min purchase amount is $1.");
        }
    }
}
