﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using Zip.Installments.Common.Model;
using Zip.InstallmentsService;

namespace Zip.InstallmentsAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PaymentPlanController : ControllerBase
    {
        #region Private Variables

        private readonly IPaymentPlanService _paymentPlanService;
        private readonly ILogger<PaymentPlanController> _logger;

        #endregion

        #region Constructor

        /// <summary>
        /// Create instance of <see cref="PaymentPlanController"/>
        /// </summary>
        /// <param name="paymentPlanService"><see cref="IPaymentPlanService"/></param>
        /// <param name="logger"><see cref="ILogger<PaymentPlanController> "/></param>
        public PaymentPlanController(IPaymentPlanService paymentPlanService, ILogger<PaymentPlanController> logger)
        {
            _paymentPlanService = paymentPlanService;
            _logger = logger;
        }

        #endregion

        #region Public Method

        /// <summary>
        /// Gets payment plan based on purchase amount
        /// </summary>
        /// <param name="purchaseAmount">The purchase amount</param>
        /// <returns><see cref="ActionResult"/></returns>
        [Route("GetPaymentPlan")]
        [HttpPost]
        public async Task<ActionResult> GetPaymentPlan([FromBody] Purchase purchase)
        {
            _logger.LogInformation($"Starting GetPaymentPlan with purchase amount:{purchase.ToString()}");

            return Ok(_paymentPlanService.CreatePaymentPlan(purchase));
        }

        #endregion
    }
}
