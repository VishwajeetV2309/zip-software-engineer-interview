using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Net;
using System.Threading.Tasks;
using Zip.Installments.Common.CustomExceptions;
using Zip.Installments.Common.Model;

namespace Zip.InstallmentsAPI.GlobalErrorHandling
{
    /// <summary>
    /// Create custom middleware for handling error.
    /// </summary>
    public class ExceptionMiddleware
    {
        #region Private Variables

        private readonly RequestDelegate _next;
        private readonly ILogger<ExceptionMiddleware> _logger;
        private readonly IWebHostEnvironment _env;

        #endregion

        #region Constructor

        /// <summary>
        /// Creates instance of <see cref="ExceptionMiddleware"/>
        /// </summary>
        /// <param name="next">The <see cref="RequestDelegate"/></param>
        /// <param name="logger">The <see cref="ILogger<ExceptionMiddleware>"/></param>
        /// <param name="env">The <see cref="IWebHostEnvironment"/></param>
        public ExceptionMiddleware(RequestDelegate next, ILogger<ExceptionMiddleware> logger, IWebHostEnvironment env)
        {
            _logger = logger;
            _env = env;
            _next = next;
        }

        #endregion

        /// <summary>
        /// Custom implementation of exception middleware
        /// </summary>
        /// <param name="httpContext"><see cref="HttpContext"/></param>
        public async Task InvokeAsync(HttpContext httpContext)
        {
            try
            {
                await _next(httpContext);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Internal server error occurred: {ex}");
                await HandleExceptionAsync(httpContext, ex);
            }
        }

        /// <summary>
        /// Handles exception 
        /// </summary>
        /// <param name="context"><see cref="HttpContext"/></param>
        /// <param name="ex"><see cref="Exception"/></param>
        private async Task HandleExceptionAsync(HttpContext context, Exception ex)
        {
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;

            ErrorInformation errorInfo;

            if (_env.IsDevelopment())
            {
                errorInfo = new ErrorInformation(context.Response.StatusCode, ex.Message, ex.StackTrace.ToString());
            }
            else
            {
                errorInfo = new ErrorInformation(context.Response.StatusCode, "Internal server error occurred.");
            }

            await context.Response.WriteAsync(errorInfo.ToString());
        }
    }
}
