﻿using Microsoft.AspNetCore.Builder;

namespace Zip.InstallmentsAPI.GlobalErrorHandling
{
    /// <summary>
    /// Used to hold extension method for exception middleware
    /// </summary>
    public static class ExceptionMiddlewareExtensions
    {
        /// <summary>
        /// Configures custom exception middleware
        /// </summary>
        /// <param name="app"><see cref="IApplicationBuilder"/></param>
        public static void ConfigureCustomExceptionMiddleware(this IApplicationBuilder app)
        {
            app.UseMiddleware<ExceptionMiddleware>();
        }
    }
}
