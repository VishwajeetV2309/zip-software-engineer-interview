using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Moq;
using Shouldly;
using System;
using System.Net;
using System.Threading.Tasks;
using Xunit;
using Zip.Installments.Common.CustomExceptions;
using Zip.Installments.Common.Model;
using Zip.InstallmentsAPI.Controllers;
using FluentValidation;
using FluentValidation.TestHelper;


namespace Zip.InstallmentsService.Test
{
    public class PaymentPlanFactoryTests
    {
        #region Private Variables 

        private Mock<ILogger<PaymentPlanController>> _logger;
        private PaymentPlanController _paymentPlanController;
        private IPaymentPlanService _paymentPlanService;
        private readonly PurchaseValidator _purchaseValidator;

        #endregion

        #region Constructor

        public PaymentPlanFactoryTests()
        {
            _logger = new Mock<ILogger<PaymentPlanController>>();
            _paymentPlanService = new PaymentPlanService(CreateOption(4, 14, 0));
            _paymentPlanController = new PaymentPlanController(_paymentPlanService, _logger.Object);
            _purchaseValidator = new PurchaseValidator();
        }

        #endregion

        #region Tests

        [Theory]
        [InlineData("0.60")]
        [InlineData("0.99")]
        public void WhenGetPaymentPlanWithInvalidOrderAmount_ShouldHaveValidationErrorForPurchaseAmount(string input)
        {
            var result = _purchaseValidator.TestValidate(new Purchase(Guid.NewGuid(), Convert.ToDecimal(input)));
            result.ShouldHaveValidationErrorFor(purchase => purchase.PurchaseAmount).WithErrorMessage("Min purchase amount is $1.");
        }

        [Theory]
        [InlineData("5")]
        [InlineData("25")]
        public void WhenGetPaymentPlanWithInvalidOrderAmount_ShouldNotHaveValidationErrorForPurchaseAmount(string input)
        {
            var result = _purchaseValidator.TestValidate(new Purchase(Guid.NewGuid(), Convert.ToDecimal(input)));
            result.ShouldNotHaveValidationErrorFor(purchase => purchase.PurchaseAmount);
        }

        [Fact]
        public async Task WhenGetPaymentPlanWithValidOrderAmount_ShouldReturnValidPaymentPlan()
        {
            // Act
            var response = await _paymentPlanController.GetPaymentPlan(new Purchase(Guid.NewGuid(), 123.45M));
            var paymentPlan = ((Microsoft.AspNetCore.Mvc.ObjectResult)response).Value as PaymentPlan;

            // Assert
            var amount = 30.86m;
            paymentPlan.ShouldNotBeNull();
            Assert.True(((Microsoft.AspNetCore.Mvc.ObjectResult)response).StatusCode == (int)HttpStatusCode.OK);
            Assert.True(paymentPlan.Installments.Length == 4);
            Assert.True(paymentPlan.Installments[0].Amount == amount);
            Assert.True(paymentPlan.Installments[1].Amount == amount);
            Assert.True(paymentPlan.Installments[2].Amount == amount);
            Assert.True(paymentPlan.Installments[3].Amount == amount);
        }

        #endregion

        #region Private Method

        private static IOptions<InstallmentSetting> CreateOption(int installments, int frequency, int startDay)
        {
            var installmentSetting = new InstallmentSetting()
            {
                Installments = installments,
                Frequency = frequency,
                StartDay = startDay
            };

            return Options.Create(installmentSetting);
        }

        #endregion
    }
}
