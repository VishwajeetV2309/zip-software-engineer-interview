using Microsoft.Extensions.Options;
using Shouldly;
using System;
using Xunit;
using Zip.Installments.Common.CustomExceptions;
using Zip.Installments.Common.Model;

namespace Zip.InstallmentsService.Test
{
    public class PaymentPlanServiceTests
    {
        #region Tests

        [Fact]
        public void WhenCreatePaymentPlanWithoutInstallmentSetting_ShouldthrowExceptionForInstallmentSetting()
        {
            try
            {
                // Arrange
                var paymentPlanService = new PaymentPlanService(null);

                // Act
                var paymentPlan = paymentPlanService.CreatePaymentPlan(new Purchase(Guid.NewGuid(), 0.45m));
            }
            catch (MissingSettingException ex)
            {
                // Assert
                Assert.True(ex.Message.Equals("Missing installment settings."));
            }
        }

        [Fact]
        public void WhenCreatePaymentPlanWithValidOrderAmount_And_InstallmentStartAfterThirtyDays_ShouldReturnValidPaymentPlan()
        {
            // Arrange
            var paymentPlanService = new PaymentPlanService(CreateOption(3, 14, 30));

            // Act
            var paymentPlan = paymentPlanService.CreatePaymentPlan(new Purchase(Guid.NewGuid(), 123.45M));

            // Assert
            paymentPlan.ShouldNotBeNull();
            Assert.True(paymentPlan.Installments.Length == 3);
            Assert.True(paymentPlan.Installments[0].DueDate.Date == DateTime.Now.AddDays(30).Date);
            Assert.True(paymentPlan.Installments[1].DueDate.Date == DateTime.Now.AddDays(44).Date);
            Assert.True(paymentPlan.Installments[2].DueDate.Date == DateTime.Now.AddDays(58).Date);
        }

        [Fact]
        public void WhenCreatePaymentPlanWithValidOrderAmount_And_ThirtyDaysFrequency_ShouldReturnValidPaymentPlan()
        {
            // Arrange
            var paymentPlanService = new PaymentPlanService(CreateOption(3, 30, 0));

            // Act
            var paymentPlan = paymentPlanService.CreatePaymentPlan(new Purchase(Guid.NewGuid(), 123.45M));

            // Assert
            paymentPlan.ShouldNotBeNull();
            Assert.True(paymentPlan.Installments.Length == 3);
            Assert.True(paymentPlan.Installments[0].DueDate.Date == DateTime.Now.Date);
            Assert.True(paymentPlan.Installments[1].DueDate.Date == DateTime.Now.AddDays(30).Date);
            Assert.True(paymentPlan.Installments[2].DueDate.Date == DateTime.Now.AddDays(60).Date);
        }

        [Fact]
        public void WhenCreatePaymentPlanWithValidOrderAmount_And_ThreeInstallment_ShouldReturnValidPaymentPlan()
        {
            // Arrange
            var paymentPlanService = new PaymentPlanService(CreateOption(3, 14, 0));

            // Act
            var paymentPlan = paymentPlanService.CreatePaymentPlan(new Purchase(Guid.NewGuid(), 123.45M));

            // Assert
            var amount = 41.15M;
            paymentPlan.ShouldNotBeNull();
            Assert.True(paymentPlan.Installments.Length == 3);
            Assert.True(paymentPlan.Installments[0].Amount == amount);
            Assert.True(paymentPlan.Installments[1].Amount == amount);
            Assert.True(paymentPlan.Installments[2].Amount == amount);
        }

        [Fact]
        public void WhenCreatePaymentPlanWithValidOrderAmount_ShouldReturnValidPaymentPlan()
        {
            // Arrange
            var paymentPlanService = new PaymentPlanService(CreateOption(4, 14, 0));

            // Act
            var paymentPlan = paymentPlanService.CreatePaymentPlan(new Purchase(Guid.NewGuid(), 123.45M));

            // Assert
            var amount = 30.86m;
            paymentPlan.ShouldNotBeNull();
            Assert.True(paymentPlan.Installments.Length == 4);
            Assert.True(paymentPlan.Installments[0].Amount == amount);
            Assert.True(paymentPlan.Installments[1].Amount == amount);
            Assert.True(paymentPlan.Installments[2].Amount == amount);
            Assert.True(paymentPlan.Installments[3].Amount == amount);
        }

        #endregion

        #region Private Method

        private static IOptions<InstallmentSetting> CreateOption(int installments, int frequency, int startDay)
        {
            var installmentSetting = new InstallmentSetting()
            {
                Installments = installments,
                Frequency = frequency,
                StartDay = startDay
            };

            return Options.Create(installmentSetting);
        }

        #endregion
    }
}
