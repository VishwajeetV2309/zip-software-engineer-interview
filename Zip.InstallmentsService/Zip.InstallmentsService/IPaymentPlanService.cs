﻿using Zip.Installments.Common.Model;

namespace Zip.InstallmentsService
{
    /// <summary>
    /// When implemented creates payment plans
    /// </summary>
    public interface IPaymentPlanService
    {
        /// <summary>
        /// Builds the PaymentPlan instance.
        /// </summary>
        /// <param name="purchaseAmount">The total amount for the purchase that the customer is making.</param>
        /// <returns>The PaymentPlan created with all properties set.</returns>
        PaymentPlan CreatePaymentPlan(Purchase purchase);
    }
}
