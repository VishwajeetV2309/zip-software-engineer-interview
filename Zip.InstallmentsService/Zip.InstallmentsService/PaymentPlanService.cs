using Microsoft.Extensions.Options;
using System;
using Zip.Installments.Common.CustomExceptions;
using Zip.Installments.Common.Model;

namespace Zip.InstallmentsService
{
    /// <summary>
    /// This class is responsible for building the PaymentPlan according to the Zip product definition.
    /// </summary>
    public class PaymentPlanService : IPaymentPlanService
    {
        #region Private Variables

        private readonly InstallmentSetting _installmentSetting;

        #endregion

        #region Constructor

        /// <summary>
        /// Creates instance of <see cref="PaymentPlanService"/>
        /// </summary>
        /// <param name="installmentSetting"></param>
        public PaymentPlanService(IOptions<InstallmentSetting> installmentSetting)
        {
            _installmentSetting = installmentSetting?.Value;
        }

        #endregion

        #region Public Method

        /// <inheritdoc/>
        public PaymentPlan CreatePaymentPlan(Purchase purchase)
        {
            if (_installmentSetting is null || _installmentSetting?.Installments == 0 || _installmentSetting?.Frequency == 0) throw new MissingSettingException("installment");

            var paymentPlan = new PaymentPlan(purchase.Id, purchase.PurchaseAmount);

            paymentPlan.Installments = new Installment[_installmentSetting.Installments];
            var installmentAmount = Math.Round(purchase.PurchaseAmount / _installmentSetting.Installments, 2);
            var currentDate = DateTime.Now.AddDays(_installmentSetting.StartDay);

            for (var i = 0; i < _installmentSetting.Installments; i++)
            {
                paymentPlan.Installments[i] = new Installment()
                {
                    Id = Guid.NewGuid(),
                    Amount = installmentAmount,
                    DueDate = currentDate.AddDays(i * _installmentSetting.Frequency).Date
                };
            }

            return paymentPlan;
        }

        #endregion
    }
}
